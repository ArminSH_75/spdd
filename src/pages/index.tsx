import type {NextPage} from 'next'
import styles from '../../styles/Home.module.css'
import Button from '@mui/material/Button'
import TextField from "../components/text-field"

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: '#d1d1d1',
        padding: "200px",
        borderRadius: "12px",
        width: "900px"
      }}>
        <Button color={'success'} size={"large"} variant={"contained"}
                sx={{boxShadow: 'none', ":hover": {boxShadow: 'none'}, marginBottom: "50px"}}>
          Enter your Age
        </Button>
        <div style={{width: "100%"}}>
        <TextField size={"large"} color={'success'} placeholder={"Enter Your Name Please!"}/>
        </div>
      </div>
    </div>
  )
}

export default Home
