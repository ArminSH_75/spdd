import React from 'react';
import TextField, {TextFieldProps} from '@mui/material/TextField';
import styles from './text-field.module.css'
import {styled} from '@mui/material/styles';

interface Props {
  className?: any
  InputProps?: any
  variant?: any
  placeholder?: any
  color?: any
  size?: any
}


const CssTextField = styled(TextField)<TextFieldProps>(({theme}) => ({
  width: "100%",
  backgroundColor: 'rgba(255,255,255,0.9)',
  justifyContent: 'center',
  borderRadius: "17px",
  '& fieldset': {
    border: 'none',
  },
  "& input": {
    fontSize: '30px'
  },
  "& .MuiInputBase-sizeSmall" : {
    height: "100px",
  }
}));

// const styles = {
//   root: {
//     backgroundColor: 'white',
//     borderRadius: "25px",
//     "& input::placeholder": {
//       "@media (max-width: 1024px)": {
//         fontSize: "16px"
//       },
//       "@media (max-width: 768px)": {
//         fontSize: "12px",
//         fontFamily: "IRANSansMedium"
//       },
//       "@media (max-width: 641px)": {
//         fontSize: "9px",
//         fontFamily: "IRANSansMedium"
//       },
//       fontSize: "19px",
//     },
//     '& label.Mui-focused': {
//       color: 'white',
//     },
//     '& .MuiInput-underline:after': {
//       borderBottomColor: 'yellow',
//     },
//     '& .MuiOutlinedInput-root': {
//       '& fieldset': {
//         border: 'none',
//       },
//       '&:hover fieldset': {
//         borderColor: 'white',
//       },
//       '&.Mui-focused fieldset': {
//         borderColor: 'yellow',
//       },
//     },
//   },
// };


const CustomTextField = (props: Props) => {
  const {className, InputProps, variant, placeholder, size, color, ...rest} = props;

  return (
    <CssTextField
      className={className}
      InputProps={InputProps}
      variant={variant}
      placeholder={placeholder}
      color={color}
      size={size}
      {...rest}/>
  );
}

export default CustomTextField;


