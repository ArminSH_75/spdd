import { createTheme } from '@mui/material/styles';
import { red, orange } from '@mui/material/colors';

const theme = createTheme({
  palette: {
    primary: {
      main: '#556cd6',
    },
    secondary: {
      main: '#19857b',
    },
    error: {
      main: red.A400,
    },
    success: {
      main: orange.A700,
      contrastText: '#ffffff'
    }
  },
});

export default theme;
